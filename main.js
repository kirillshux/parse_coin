const fetch = require('node-fetch');
const cheerio = require('cheerio');

function getHtml() {
  return fetch('https://coinmarketcap.com/coins/')
      .then(function(res) {
          return res.text();
      });  
}

function parseHtml(html) {
  var result = [];
  var $ = cheerio.load(html);

  $('#currencies tbody tr').each((i, elem) => {
    var tr = $(elem);
    var position = tr.find('td:first-child').text().replace(/\s*/g, '');
    var coinNamed = tr.find('.currency-name-container').text();
    var market = tr.find('.market-cap').text().trim();
    var price = parseFloat(tr.find('.price').text().replace(/[^\d\.]/, ''));
    var circSupply = parseFloat(tr.find('.circulating-supply > a').attr('data-supply'));
    var index = circSupply / price;

    if (isNaN(circSupply)) {
      circSupply = parseFloat(tr.find('.stale').attr('data-supply'));
    }

    result.push({
      position,
      coinNamed,
      market,
      price,
      circSupply,
      index
    });

  });

  return result;
}

getHtml()
  .then(parseHtml)
  .then(objects => {
    console.log('Objects', objects);
    return objects.sort((a, b) => {
      return a.index - b.index;
    });
  })
  .then(console.log);

// console.log(parseHtml())

// console.log(result)

// osmosis
//   .get('www.craigslist.org/about/sites')
//   .find('h1 + div a')
//   .set('location')
//   .follow('@href')
//   .find('header + div + div li > a')
//   .set('category')
//   .data(function(data) {
//       console.log(data);
//   })